const request = require('supertest');
const {ObjectId} = require('mongodb');
const app = require('../server.js');

const data = [
  {
    _id: new ObjectId(),
    content: 'test content',
    title: 'test',
    upvotes: 1,
    downvotes: 0,
    author: 'Test Me',
    lastModified: new Date(),
  },
  {
    _id: new ObjectId(),
    content: 'second test content',
    title: 'second test',
    upvotes: 10,
    downvotes: 20,
    author: 'Second Test Me',
    lastModified: new Date(),
  },
];

// wait for connection
beforeAll(done => app.on('setup_finished', () => done()));

beforeEach(async () => app.locals.db.collection('posts').deleteMany({}));
beforeEach(async () => app.locals.db.collection('posts').insertMany(data));

describe('POST /posts', () => {
  it('should create a new post', async () => {
    const post = {
      content: 'new test content',
      title: 'new test',
      upvotes: 3,
      downvotes: 9,
      author: 'New Test User',
      lastModified: new Date(),
    };

    const res = await request(app)
      .post('/posts')
      .send(post);

    expect(res.statusCode).toEqual(201);
    expect(res.body.lines).toBe('1 lines inserted');

    const result = await app.locals.db
      .collection('posts')
      .findOne({title: 'new test'});

    expect(result.content).toBe(post.content);
  });
});

describe('POST /posts/vote', () => {
  it('should increase upvotes', async () => {
    const {_id} = data[0];
    const increasedVote = data[0].upvotes + 1;
    const res = await request(app)
      .post('/posts/vote')
      .send({
        field: 'upvotes',
        post: {
          _id: data[0]._id,
          content: data[0].content,
          title: data[0].title,
          upvotes: data[0].upvotes + 1,
          downvotes: data[0].downvotes,
          author: data[0].author,
          lastModified: data[0].lastModified,
        },
      });

    expect(res.statusCode).toEqual(200);
    expect(res.body.lines).toBe('1 lines updated');

    const result = await app.locals.db.collection('posts').findOne({_id});

    expect(result.upvotes).toBe(increasedVote);
  });

  it('should increase downvotes', async () => {
    const {_id} = data[1];
    const increasedVote = data[1].downvotes + 1;
    const res = await request(app)
      .post('/posts/vote')
      .send({
        field: 'downvotes',
        post: {
          _id: data[1]._id,
          content: data[1].content,
          title: data[1].title,
          upvotes: data[1].upvotes,
          downvotes: data[1].downvotes + 1,
          author: data[1].author,
          lastModified: data[1].lastModified,
        },
      });

    expect(res.statusCode).toEqual(200);
    expect(res.body.lines).toBe('1 lines updated');

    const result = await app.locals.db.collection('posts').findOne({_id});

    expect(result.downvotes).toBe(increasedVote);
  });
});

describe('GET /posts', () => {
  it('should return all posts in db', async () => {
    const res = await request(app).get('/posts');

    expect(res.statusCode).toEqual(200);
    expect(res.body.posts.length).toBe(2);
  });
});

describe('DELETE /posts/:id', () => {
  it('should remove a post', async () => {
    const {_id} = data[1];

    const res = await request(app).delete(`/posts/${_id}`);

    expect(res.statusCode).toEqual(200);
    expect(res.body.lines).toBe('1 lines deleted');

    const result = await app.locals.db.collection('posts').findOne({_id});

    expect(result).toBeFalsy();
  });
});

describe('/PUT /posts/:id', () => {
  it('should update the post content', async () => {
    const {_id} = data[0];
    const newContent = {content: 'I have been murdered', title: 'Attention'};

    const res = await request(app)
      .put(`/posts/${_id}`)
      .send({content: newContent});

    expect(res.statusCode).toEqual(200);
    expect(res.body.lines).toBe('1 lines updated');

    const result = await app.locals.db.collection('posts').findOne({_id});

    expect(result.content).not.toEqual(data[0].content);
    expect(result.title).not.toEqual(data[0].title);
  });
});
