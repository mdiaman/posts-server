## Server & Database

### Starting the services
Running `docker-compose up` will start a MongoDB instance and an ExpressJS server listening on port 3000.

### Testing
Running `yarn testinit` will start the test database and execute jest.