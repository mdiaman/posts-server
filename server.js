const express = require('express');
const bodyParser = require('body-parser');
const query = require('./db/queries');
const connect = require('./db/connection');

const app = express();

app.use(bodyParser.json());

/**
 * Routes
 */
app.get('/', (req, res) => res.send("I'm up!"));
app.get('/posts', query.getPosts);
app.post('/posts', query.addPost);
app.post('/posts/vote', query.vote);
app.put('/posts/:id', query.updatePost);
app.delete('/posts/:id', query.deletePost);

// Connect to db
connect()
  .then(db => {
    // Store connection to db
    app.locals.db = db;

    // Start server
    app.listen(3000, () => {
      console.log(
        '\x1b[36m%s\x1b[0m',
        'All done setting up server and database!',
      );
      app.emit('setup_finished');
    });
  })
  .catch(err => {
    console.log(err);
  });

module.exports = app;
