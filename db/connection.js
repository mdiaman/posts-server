require('dotenv').config();
const {MongoClient} = require('mongodb');
const seedDB = require('./setup');

const connect = async () => {
  const database =
    process.env.NODE_ENV === 'test' ? 'postsapptest' : 'postsapp';
  const url = `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@db:27017/${database}?authMechanism=SCRAM-SHA-1&authSource=admin`;

  const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  // Connect
  await client.connect();
  console.log('Acquired connection');

  // Open database
  const db = client.db(database);

  // Seed db with initial data
  if (process.env.NODE_ENV !== 'test') await seedDB(db);

  return db;
};

module.exports = connect;
