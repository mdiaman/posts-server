const seedDB = async db => {
  let msg;
  try {
    await db.collection('posts').deleteMany({});

    const results = await db.collection('posts').insertMany([
      {
        content: 'lorem ipsum etc',
        title: 'this is a title',
        upvotes: 2,
        downvotes: 0,
        author: 'Me',
        lastModified: new Date(),
      },
      {
        content: 'cool goat',
        title: 'CHARGE',
        upvotes: 7,
        downvotes: 1,
        author: 'Kassandra of Sparta',
        lastModified: new Date(),
      },
      {
        content: 'sign my petition to abolish the law of surprise',
        title: 'please',
        upvotes: 21,
        downvotes: 2,
        author: 'Geralt of Rivia',
        lastModified: new Date(),
      },
      {
        content: 'user also made this post',
        title: 'this is another title',
        upvotes: 2,
        downvotes: 0,
        author: 'Me',
        lastModified: new Date(),
      },
      {
        title: 'another one',
        content: 'something something',
        upvotes: 2,
        downvotes: 0,
        author: 'DJ Khaled',
        lastModified: new Date(),
      },
    ]);
    msg = `seeded db with ${results.insertedCount} rows`;
  } catch (err) {
    console.log(err.stack);
    throw new Error(`error seeding db: ${err.name}`);
  }
  return msg;
};

module.exports = seedDB;
