const {ObjectId} = require('mongodb');

const getPosts = async (req, res) => {
  const posts = req.app.locals.db
    .collection('posts')
    .find({}, {sort: [['lastModified', -1]]});
  const values = await posts.toArray();
  res.status(200).json({posts: values});
};

const addPost = async (req, res) => {
  const {title, content, author} = req.body;

  const result = await req.app.locals.db.collection('posts').insertOne({
    title,
    content,
    author,
    upvotes: 0,
    downvotes: 0,
    lastModified: new Date(),
  });

  res.status(201).send({lines: `${result.insertedCount} lines inserted`});
};

const updatePost = async (req, res) => {
  const _id = new ObjectId(req.params.id);
  const {title, content} = req.body;

  const result = await req.app.locals.db
    .collection('posts')
    .updateOne({_id}, {$set: {title, content, lastModified: new Date()}});
  res.status(200).send({lines: `${result.modifiedCount} lines updated`});
};

const deletePost = async (req, res) => {
  const _id = new ObjectId(req.params.id);
  const result = await req.app.locals.db.collection('posts').deleteOne({_id});
  res.status(200).send({lines: `${result.deletedCount} lines deleted`});
};

const vote = async (req, res) => {
  const {field, post} = req.body;
  const _id = new ObjectId(post._id);
  const op =
    field === 'upvotes'
      ? {$set: {upvotes: post.upvotes, lastModified: post.lastModified}}
      : {$set: {downvotes: post.downvotes, lastModified: post.lastModified}};

  const result = await req.app.locals.db
    .collection('posts')
    .updateOne({_id}, op);
  res.status(200).send({lines: `${result.modifiedCount} lines updated`});
};

module.exports = {
  getPosts,
  addPost,
  updatePost,
  deletePost,
  vote,
};
